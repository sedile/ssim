package gui;

import network.Client;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ClientGUI extends Stage {

	private Client client;
	private TextArea chattext;
	private TextField chatentry;
	private Button sendBtn;
	
	private String CHATNAME, HOSTNAME;
	private final int SERVERPORT;
	
	public ClientGUI(String chatname, String serverIP, int serverport){
		CHATNAME = chatname;
		HOSTNAME = serverIP;
		SERVERPORT = serverport;
		Scene scene = new Scene(initComponents());
		setScene(scene);
		setTitle("SSIM - Client : Nachrichtensitzung");
		sizeToScene();
		setResizable(false);
		setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent we) {
				client.stopClient();
				Platform.exit();
				System.exit(0);
			}		
		});

		show();
		
		client = new Client(HOSTNAME, SERVERPORT);
		client.association(this);
		client.setDaemon(true);
		client.startClient();
	}
	
	private Parent initComponents(){
		chatentry = new TextField();
		chatentry.setEditable(false);
		chatentry.setPrefSize(400,25);
		
		chattext = new TextArea();
		chattext.setEditable(false);
		chattext.setWrapText(false);
		ScrollPane scroll = new ScrollPane(chattext);

		sendBtn = new Button("Senden");
		sendBtn.setOnAction(new Handler());
		
		FlowPane flow = new FlowPane();
		flow.getChildren().addAll(scroll, new Label(CHATNAME), chatentry, sendBtn);
		return flow;
	}

	public void clearTextArea(){
		chattext.clear();
	}
	
	public void clearChatEntry(){
		chatentry.clear();
	}
	
	public String getChatname(){
		return CHATNAME;
	}
	
	public void showMessage(final String text){
		chattext.appendText(text+"\n");
	}
	
	public void allowToWrite(final boolean erlauben){
		chatentry.setEditable(erlauben);
	}

	private class Handler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent action) {
			if ( !chatentry.getText().isEmpty() && !chatentry.getText().trim().isEmpty() ) {
				client.sendMessage(chatentry.getText());
				clearChatEntry();
			}
		}

	}

}
