package gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainMenu extends Application {
	
	private Stage stage;
	private RadioButton client, server;
	private Label hostlabel, portlabel, namelabel;
	private TextField hostField, portField, nameField;
	private Button confirm;

	private boolean isServer = false;
	
	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		initComponents();
		initLayout();
	}
	
	@Override
	public void stop() {
		Platform.exit();
	}

	private void initComponents(){
		client = new RadioButton("Client");
		client.setSelected(true);
		client.setOnAction(new Handler());
		server = new RadioButton("Server");
		server.setOnAction(new Handler());
		hostlabel = new Label("Host");
		portlabel = new Label("Port");
		namelabel = new Label("Alias");
		hostField = new TextField("");
		portField = new TextField("");
		nameField = new TextField("");
		confirm = new Button("confirm");
		confirm.setOnAction(new Handler());
	}
	
	private void initLayout(){
		GridPane layout = new GridPane();
		layout.add(client, 0, 0);
		layout.add(server, 1, 0);
		layout.add(hostlabel, 0, 1);
		layout.add(hostField, 1, 1);
		layout.add(portlabel, 0, 2);
		layout.add(portField, 1, 2);
		layout.add(namelabel, 0, 3);
		layout.add(nameField, 1, 3);
		layout.add(confirm, 0, 4);

		Scene scene = new Scene(layout);
		stage.setScene(scene);
		stage.sizeToScene();
		stage.setResizable(false);
		stage.setTitle("Simple Secure Instant Messenger");
		stage.show();
	}
	
	private void examine(){
		if ( isServer ){
			
			String chatname = nameField.getText();
			if ( chatname.isEmpty() || chatname.length() > 12 ){
				chatname = "SERVER";
			}
			
			try {
				int port = Integer.parseInt(portField.getText());
				
				if( port < 0 || port > 65535){
					Alert info = new Alert(AlertType.INFORMATION);
					info.setHeaderText("Info");
					info.setContentText("Nur Zahlen zwischen 0 und 65535 erlaubt");
					info.showAndWait();
				}
				
				new ServerGUI(chatname, port);
				stage.close();
			} catch ( NumberFormatException ne ){
				Alert info = new Alert(AlertType.INFORMATION);
				info.setHeaderText("Info");
				info.setContentText("Als Port duerfen nur Zahlen eingegeben werden");
				info.showAndWait();
				portField.setText("1024");
			}
		} else {
			
			String chatname = nameField.getText();
			if ( chatname.isEmpty() || chatname.length() > 12 ){
				chatname = "CLIENT";
			}
			
			String hostname = hostField.getText();
			if( hostname.isEmpty() || hostname.length() > 32){
				Alert info = new Alert(AlertType.INFORMATION);
				info.setHeaderText("Info");
				info.setContentText("Der Hostname ist leer oder zu lang");
				info.showAndWait();
			}
			
			try {
				int port = Integer.parseInt(portField.getText());
				if( port < 0 || port > 65535){
					Alert info = new Alert(AlertType.INFORMATION);
					info.setHeaderText("Info");
					info.setContentText("Nur Zahlen zwischen 0 und 65535 erlaubt");
					info.showAndWait();
				}
				
				new ClientGUI(chatname, hostname, port);
				stage.close();
			} catch ( NumberFormatException ne ){
				Alert info = new Alert(AlertType.INFORMATION);
				info.setHeaderText("Info");
				info.setContentText("Als Port duerfen nur Zahlen eingegeben werden");
				info.showAndWait();
				portField.setText("1024");
			}
		}
	}
	
	private class Handler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent ae) {
			if( ae.getSource() == confirm ){
				examine();
			} else if ( ae.getSource() == client ){
				client.setSelected(true);
				server.setSelected(false);
				isServer = false;
			} else if ( ae.getSource() == server ){
				client.setSelected(false);
				server.setSelected(true);
				isServer = true;
			}	
		}
	}
	
	public static void main(String[] args){
		launch();
	}
}
