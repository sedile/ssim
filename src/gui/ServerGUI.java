package gui;

import network.Server;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ServerGUI extends Stage {
	
	private Server server;
	private TextArea chattext;
	private TextField chatentry;
	private Button sendBtn;
	
	private final String CHATNAME;
	private final int SERVERPORT;
	
	public ServerGUI(String chatname, int serverport){
		CHATNAME = chatname;
		SERVERPORT = serverport;
		Scene scene = new Scene(initComponents());
		setScene(scene);
		setTitle("SSIM - Server : Nachrichtensitzung");
		sizeToScene();
		setResizable(false);
		
		setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent we) {
				server.stopServer();
				Platform.exit();
				System.exit(0);
			}	
		});
		
		show();

		server = new Server(SERVERPORT);
		server.association(this);
		server.setDaemon(true);
		server.startServer();
	}

	private Parent initComponents(){
		chatentry = new TextField();
		chatentry.setEditable(false);
		chatentry.setPrefSize(400,25);
		
		chattext = new TextArea();
		chattext.setEditable(false);
		chattext.setWrapText(false);
		ScrollPane scroll = new ScrollPane(chattext);

		sendBtn = new Button("Senden");
		sendBtn.setOnAction(new Handler());
		
		FlowPane flow = new FlowPane();
		flow.getChildren().addAll(scroll, new Label(CHATNAME), chatentry, sendBtn);
		return flow;
	}
	
	public void clearTextArea(){
		chattext.clear();
	}
	
	public void clearChatEntry(){
		chatentry.clear();
	}

	public String getChatname(){
		return CHATNAME;
	}
	
	public void getServerport(){
		server.getServerPort();
	}
	
	public void showMessage(final String text){
		chattext.appendText(text+"\n");
	}
	
	public void allowToWrite(final boolean erlauben){
		chatentry.setEditable(erlauben);
	}

	private class Handler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent action) {
			if ( !chatentry.getText().isEmpty()  && !chatentry.getText().trim().isEmpty() ) {
				server.sendMessage(chatentry.getText() );
				clearChatEntry();
			}
		}	
	}
}
