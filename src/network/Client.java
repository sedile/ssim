package network;

import gui.ClientGUI;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

import root.Text;
import root.Protocol;
import security.AES;
import security.RSA;

public class Client extends Thread {
	
	private ClientGUI gui;
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;

	private KeyPair keypair;
	private PublicKey serverkey;
	private SecretKeySpec sessonkey;

	private String ip;
	private int port;
	
	public Client(String ip, int port){
		this.ip = ip;
		this.port = port;
	}
	
	public void association(ClientGUI gui){
		this.gui = gui;
		generateKeyPair();
	}
	
	private void generateKeyPair(){
		gui.showMessage("<Generiere ein Schluesselpaar...>");
		keypair = Protocol.generateKeyPair();
	}
	
	private String getMessageAuthenticationCode(String msg){
		return Protocol.getMessageAuthenticationCode(msg, sessonkey);
	}
	
	public void startClient(){
		start();
	}
	
	public void stopClient(){
		gui.allowToWrite(false);
		interrupt();
	}

	@Override
	public void run() {
		try {
			connectToServer();
			startStreams();
			exchangeKeys();
			chat();
		} catch ( InterruptedException ie){
			ie.printStackTrace();
			gui.showMessage("<FEHLER : Threadfehler>");
			stopClient();
		} catch ( SocketException se ){
			se.printStackTrace();
			gui.showMessage("<FEHLER : Fehler beim Verbindungsaufbau>");
			stopClient();
		} catch ( Exception e ){
			e.printStackTrace();
			gui.showMessage("<FEHLER : Fehler waehrend der Verbindung>");
			stopClient();
		}
	}
	
	private void connectToServer() throws Exception {
		gui.showMessage("<Schluesselpaar generiert. Warte auf Antwort vom Server...>");
		socket = new Socket(ip,port);
		gui.showMessage("<Verbunden mit : "+socket.getRemoteSocketAddress()+">");
	}
	
	private void startStreams() throws Exception {
		output = new ObjectOutputStream(socket.getOutputStream());
		input = new ObjectInputStream(socket.getInputStream());
		gui.showMessage("<Streams wurden initialisiert>");
	}
	
	private void exchangeKeys() throws Exception {
		output.writeObject((String) "RegisterForChat()");
		output.flush();
		String ok = (String) input.readObject();
		if( ok.equals("confirm(ERROR)")){
			gui.showMessage("<FEHLER : Fehler beim Protokoll : confirm(ERROR)>");
			socket.close();
		} else if ( ok.equals("confirm(OK)")){
			gui.showMessage("Tausche noetige Schluessel aus...");
			serverkey = RSA.stringZuSchluessel((String) input.readObject());
			output.writeObject((String) RSA.schluesselAlsString(keypair.getPublic()));
			sessonkey = AES.stringZuSchluessel(RSA.entschluesseln((byte[]) input.readObject(), keypair.getPrivate()));
		}
	}
	
	private void chat() {
		gui.allowToWrite(true);
		while(true) {		
			try {
				Text t = (Text) input.readObject();			
				String msg = t.getMessage();
				String mac = t.getMAC();
				String message = AES.entschluesseln(msg, sessonkey);
				String examine_mac = getMessageAuthenticationCode(message);
				if ( !mac.equals(examine_mac) ){
					gui.showMessage("<HINWEIS : Ungleicher MAC>");
				}
				examinePattern(message);
			} catch ( ClassNotFoundException cnfe){
				cnfe.printStackTrace();
				gui.showMessage("<FEHLER : Der Eingegebene Text ist ungueltig>");
				gui.allowToWrite(false);
			} catch ( SocketException se ){
				gui.showMessage("<FEHLER : Verbindung wurde getrennt>");
				se.printStackTrace();
				quit();
				break;
			} catch ( EOFException eofe ){
				gui.showMessage("<INFO : Verbindung wurde vom Server beendet>");
				eofe.printStackTrace();
				quit();
				break;
			} catch ( IOException ioe ){
				gui.showMessage("<INFO : Stromfehler>");
				ioe.printStackTrace();
				quit();
				break;
			}
		}
	}
	
	private void examinePattern(String msg){
		if(Protocol.quitChat(msg)){
			gui.showMessage("<Der Server hat die Verbindung beendet...>");
			quit();
		} else {
			gui.showMessage(msg);
			msg = "";
		}
	}
	
	private void quit(){
		gui.allowToWrite(false);
		try {
			if ( output != null ){
				output.close();
			}
			if ( input != null ){
				input.close();
			}
			if ( socket != null){
				socket.close();
			}
		} catch ( Exception e ){
			e.printStackTrace();
		} finally {
			stopClient();
		}
	}
	
	public void sendMessage(String text) {
		
		if ( Protocol.quitChat(text) ){
			quit();
		} else if ( Protocol.clearChat(text) ){
			gui.clearTextArea();
			gui.clearChatEntry();
			return;
		}
		
		try {
			String msg = "\n"+gui.getChatname()+" ["+new SimpleDateFormat().format(new Date())+"]\n"+text;
			String sec = AES.verschluesseln(msg, sessonkey);
			String mac = getMessageAuthenticationCode(msg);
			Text t = new Text(mac, sec);
			output.writeObject((Text) t);
			output.flush();
			gui.showMessage(msg);
		} catch ( IOException ioe ){
			ioe.printStackTrace();
			gui.showMessage("<FEHLER : Senden fehlgeschlagen>");
		}
	}
	
	public String getServerIP(){
		return ip;
	}
	
	public int getServerPort(){
		return port;
	}
}
