package network;

import gui.ServerGUI;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

import root.Protocol;
import root.Text;

import security.AES;
import security.RSA;

public class Server extends Thread {
	
	private ServerGUI gui;
	private ServerSocket server;
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	
	private KeyPair keypair;
	private PublicKey clientkey;
	private SecretKeySpec sessonkey;

	private int port;
	
	public Server(int port){
		this.port = port;
	}
	
	public void association(ServerGUI gui){
		this.gui = gui;
		generateKeyPair();
		generateSessonKey();
	}
	
	private void generateKeyPair(){
		gui.showMessage("<Generiere ein Schluesselpaar...>");
		keypair = Protocol.generateKeyPair();
	}

	private void generateSessonKey(){
		sessonkey = Protocol.generateSessonKey();
	}
	
	private String getMessageAuthenticationCode(String msg){
		return Protocol.getMessageAuthenticationCode(msg, sessonkey);
	}
	
	public void stopServer(){
		gui.allowToWrite(false);
		interrupt();
	}

	public void startServer() {
		start();
	}
	
	@Override
	public void run(){
		try {
			server = new ServerSocket(port);
			server.setSoTimeout(60000);
			
			while(!isInterrupted()){
				try{
					waitForClient();
					startStreams();
					exchangeKeys();
					chat();
				} catch ( InterruptedException ie){
					ie.printStackTrace();
					gui.showMessage("<FEHLER : Threadfehler>");
					stopServer();
				} catch ( SocketTimeoutException stoe){
					stoe.printStackTrace();
					gui.showMessage("<FEHLER : Socket Timeout>");
					stopServer();
				} catch ( Exception e ){
					e.printStackTrace();
					gui.showMessage("<FEHLER : Fehler wawhrend der Verbindung>");
					stopServer();
				}
			}
			
		} catch ( IOException ioe ){
			gui.showMessage("<FEHLER : Es konnte kein Server erstellt werden>");
			stopServer();
		}
	}
	
	private void waitForClient() throws Exception {
		gui.showMessage("<Schluesselpaar generiert. Warte 60 Sekunden auf eine eingehende Verbindung...>");
		socket = server.accept();
		gui.showMessage("<Verbunden mit : "+socket.getRemoteSocketAddress()+">");
	}
	
	private void startStreams() throws Exception {
		input = new ObjectInputStream(socket.getInputStream());
		output = new ObjectOutputStream(socket.getOutputStream());
		gui.showMessage("<Streams wurden initialisiert>");
	}
	
	private void exchangeKeys() throws Exception {
		String reg = (String) input.readObject();
		if( Protocol.registerForChat(reg)){
			output.writeObject((String) Protocol.confirmMessage(true));
			output.flush();
			gui.showMessage("<Tausche noetige Schluessel aus...>");
			output.writeObject((String) RSA.schluesselAlsString(keypair.getPublic()));
			output.flush();
			clientkey = RSA.stringZuSchluessel((String) input.readObject());
			byte[] sessonbytes = RSA.verschluesseln(AES.schluesselAlsString(sessonkey), clientkey);
			output.writeObject(sessonbytes);
			output.flush();
		} else {
			output.writeObject((String) Protocol.confirmMessage(false));
			output.flush();
			gui.showMessage("<FEHLER : Fehler beim Protokoll :"+reg+">");
			socket.close();
		}
	}
	
	private void chat() {
		String message = "<Verbindung wurde aufgebaut>";
		sendMessage(message);
		gui.allowToWrite(true);
		while(true) {
			try {			
				Text t = (Text) input.readObject();
				String msg = t.getMessage();
				String mac = t.getMAC();
				message = AES.entschluesseln(msg, sessonkey);
				String examine_mac = getMessageAuthenticationCode(message);
				if ( !mac.equals(examine_mac) ){
					gui.showMessage("<HINWEIS : Ungleicher MAC>");
				}
				examinePattern(message);
				
			} catch ( ClassNotFoundException cnfe ){
				gui.showMessage("<FEHLER : Der Eingegebene Text ist ungueltig>");
				gui.allowToWrite(false);
			} catch ( SocketException se ){ 
				gui.showMessage("<INFO : Verbindung wurde getrennt>");
				se.printStackTrace();
				quit();
				break;
			} catch ( EOFException eofe ){
				gui.showMessage("<INFO : Verbindung wurde vom Client beendet>");
				eofe.printStackTrace();
				quit();
				break;
			} catch ( IOException ioe ){
				gui.showMessage("<INFO : Stromfehler>");
				ioe.printStackTrace();
				quit();
				break;
			}
		}
	}
	
	private void examinePattern(String msg){
		if(Protocol.quitChat(msg)){
			gui.showMessage("<Der Client hat die Verbindung beendet...>");
			quit();
		} else {
			gui.showMessage(msg);
			msg = "";
		}
	}
	
	public void quit(){
		gui.allowToWrite(false);
		try {
			if ( output != null ){
				output.close();
			}
			if ( input != null ){
				input.close();
			}
			if ( socket != null){
				socket.close();
			}
		} catch ( Exception e ){
			e.printStackTrace();
		} finally {
			stopServer();
		}
	}
	
	public void sendMessage(String text) {
		
		if ( Protocol.quitChat(text) ){
			quit();
		} else if ( Protocol.clearChat(text) ){
			gui.clearTextArea();
			gui.clearChatEntry();
			return;
		}
		
		try {
			String msg = "\n"+gui.getChatname()+" ["+new SimpleDateFormat().format(new Date())+"]\n"+text;			
			String sec = AES.verschluesseln(msg, sessonkey);
			String mac = getMessageAuthenticationCode(msg);
			Text t = new Text(mac, sec);
			output.writeObject((Text) t);
			output.flush();
			gui.showMessage(msg);
		} catch ( IOException ioe ){
			ioe.printStackTrace();
			gui.showMessage("<FEHLER : Senden fehlgeschlagen>");
		} 
	}

	public int getServerPort(){
		return port;
	}
}
