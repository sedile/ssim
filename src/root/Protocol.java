package root;

import java.security.KeyPair;

import javax.crypto.spec.SecretKeySpec;

import security.*;

/**
 * @author sebastian
 * Communication Protocol between Client and Server
 */
public class Protocol {
	
	public static KeyPair generateKeyPair(){
		KeyPair keypair = RSA.genSchluessel(2048);
		return keypair;
	}
	
	public static SecretKeySpec generateSessonKey(){
		SecretKeySpec sessonkey = AES.genSitzungsschluessel();
		return sessonkey;
	}
	
	public static String getMessageAuthenticationCode(String message, SecretKeySpec sessonkey){
		String hash = Hashing.getHash(message);
		String mac = AES.verschluesseln(hash, sessonkey);
		return mac;
	}

	public static boolean registerForChat(String request){
		if(request.equals("RegisterForChat()")){
			return true;
		}
		return false;
	}
	
	public static String confirmMessage(boolean ok){
		if( ok ){
			return new String("confirm(OK)");
		} else {
			return new String("confirm(ERROR)");
		}
	}
	
	public static boolean quitChat(String text){
		if( text.contains("!shut")){
			return true;
		}
		return false;
	}
	
	public static boolean clearChat(String text){
		if( text.contains("!clear")){
			return true;
		}
		return false;
	}
}
