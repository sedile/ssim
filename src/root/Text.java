package root;

import java.io.Serializable;

/**
 * @author sebastian
 * A block is a Tuple of a mac and data
 */
public class Text implements Serializable {

	private static final long serialVersionUID = 3034560726864235348L;
	private final String MAC, MSG;
	
	/**
	 * A block contains the plain message and the message authentication code
	 * @param mac mac of the plaintext
	 * @param data the data like messages
	 */
	public Text(String mac, String msg){
		MAC = mac;
		MSG = msg;
	}
	
	/**
	 * @return mac of the plaintext
	 */
	public String getMAC(){
		return MAC;
	}
	
	/**
	 * 
	 * @return the data
	 */
	public String getMessage(){
		return MSG;
	}

}
