package security;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public final class AES {

	/**
	 * erzeugt ein Sitzungsschluessel mit den Verfahren AES
	 * @return der Sitzungsschluessel
	 */
	public static SecretKeySpec genSitzungsschluessel(){
		String roundkey = genRundenschluessel();
		SecretKeySpec sessonkey = genSitzungsschluessel(roundkey);
		return sessonkey;
	}
	
	/**
	 * generiert ein zufaelligen String fuer den Rundenschluessel 
	 * @return zufaelliger String
	 */
	private static String genRundenschluessel(){
		String roundkey = new String("");
		for(int n = 0; n < Byte.MAX_VALUE; n++){
			int random = (int) (Math.random() * 94) + 33;
			roundkey += (char) random;
		}
		return roundkey;
	}
	
	/**
	 * generiert den Sesson-Key fuer die Kommunikation, um ein Klartext
	 * mit diesen Schluessel zu verschluesseln
	 * @param schluesseltext generierter String
	 * @return Session-Key
	 */
	private static SecretKeySpec genSitzungsschluessel(String schluesseltext){
		byte[] schluessel = Hashing.getHashBytes(schluesseltext);

	    /* nur die ersten 128 bit nutzen */
	    schluessel = Arrays.copyOf(schluessel, 16);
	    
	    /* Der fertige Schluessel fuer AES */
	    SecretKeySpec secretKeySpec = new SecretKeySpec(schluessel, "AES");
	    return secretKeySpec;
	}
	
	/**
	 * verschluesselt den Klartext mit den generierten Sesson-Key
	 * @param klartext unverschluesselte Nachricht, die verschluesselt werden soll
	 * @param key der Sesson-Key
	 * @return verschluesselter Text
	 */
	public static String verschluesseln(String klartext, SecretKeySpec key) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		}
		
		byte[] encrypthed = null;
		try {
			encrypthed = cipher.doFinal(klartext.getBytes());
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}

	  	String geheim = Base64.getEncoder().encodeToString(encrypthed);
		return geheim;
	}
	
	/**
	 * entschluesselt den Geheimtext mit den Sesson-Key
	 * @param geheimtext verschluesselter Text
	 * @param key der Sesson-Key
	 * @return entschluesselter Text
	 */
	public static String entschluesseln(String geheimtext, SecretKeySpec key){
	  	byte[] decValue = Base64.getDecoder().decode(geheimtext);
	  	
	    Cipher cipher = null;
	    try {
	    	cipher = Cipher.getInstance("AES");
	    	cipher.init(Cipher.DECRYPT_MODE, key);
	    } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
	    	e.printStackTrace();
	    }
	    
	    byte[] decrypthed = null;
		try {
			decrypthed = cipher.doFinal(decValue);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
	    return new String(decrypthed);
	}
	
	/**
	 * konvertiert den Secret-Key als ein String
	 * @param sessonkey der Sesson-Key
	 * @return String des Sesson-Keys
	 */
	public static String schluesselAlsString(SecretKeySpec sessonkey){
		String sessonkeystring = Base64.getEncoder().encodeToString(sessonkey.getEncoded());
		return sessonkeystring;
	}
	
	/**
	 * konvertiert den String zu ein Sesson-Key
	 * @param sessonkeystring String des Sessonkeys
	 * @return der Sessonkey des Strings
	 */
	public static SecretKeySpec stringZuSchluessel(String sessonkeystring){
		byte[] key = Base64.getDecoder().decode(sessonkeystring);
		return new SecretKeySpec(key,0,key.length,"AES");
	}
}
