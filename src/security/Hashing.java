package security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Hashing {

	/**
	 * Berechnet zu einer Texteingabe ein Hashwert und gibt ihn als String zurueck
	 * @param text texteingabe
	 * @return hashwert des Strings
	 */
	public static String getHash(String text){
		String hashwert = generiereHashwert(text);
		return hashwert;
	}
	
	/**
	 * Berechnet zu einer Texteingabe ein Hashwert in Bytes und gibt ihn als bytearray zurueck
	 * @param text texteingabe
	 * @return hashbytes
	 */
	public static byte[] getHashBytes(String text){
		byte[] hashbytes = generiereHashBytes(text);
		return hashbytes;
	}

	/**
	 * generiert zu einer Texteingabe ein Hashwert
	 * @param text text
	 * @return der hashwert
	 */
	private static String generiereHashwert(String text){
		try {
			byte[] textbytes = text.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-512");
			sha.update(textbytes);
			byte[] shabytes = sha.digest();
			String hashwert = DatatypeConverter.printHexBinary(shabytes);
			return hashwert.toLowerCase();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
			return new String("ERROR");
		}
	}
	
	/**
	 * generiert zu einer Texteingabe ein Hashwert
	 * @param text text
	 * @return der hashwert
	 */
	private static byte[] generiereHashBytes(String text){
		try {
			byte[] textbytes = text.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-512");
			sha.update(textbytes);
			byte[] shabytes = sha.digest();
			return shabytes;
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
			return new byte[]{0};
		}
	}
}
