package security;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public final class RSA {

	/**
	 * Erzeugt ein Schluesselpaar mit der uebergebenen Schluessellaenge
	 * @param schluessellaenge die Schluesselmenge
	 */
	public static KeyPair genSchluessel(int schluessellaenge){
		KeyPair keypair = genSchluesselpaar(schluessellaenge);
		return keypair;
	}
	
	/**
	 * generiert ein Schluesselpaar bestehend aus Public Key und Private Key
	 * @param keysize die Schluesselmenge
	 */
	private static KeyPair genSchluesselpaar(int keysize){
		KeyPairGenerator keygen = null;
		try {
			keygen = KeyPairGenerator.getInstance("RSA");
		} catch ( NoSuchAlgorithmException e ){
			e.printStackTrace();
		}
		
		keygen.initialize(keysize);
		KeyPair key = keygen.generateKeyPair();
		return key;
	}
	
	/**
	 * verschluesselt den Klartex mit den PublicKey
	 * @param klartext Die Klartextnachricht
	 * @param pk der PublicKey
	 * @return das Chiffrat
	 */
	public static byte[] verschluesseln(String klartext, PublicKey pk){
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, pk);
		} catch ( NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e ){
			e.printStackTrace();
		}
		
		byte[] chiffrat = null;
		try {
			chiffrat = cipher.doFinal(klartext.getBytes());
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		return chiffrat;
	}
	
	/**
	 * entschluesselt das Chiffrat mit den PrivateKey
	 * @param chiffrat das Chiffrat zur entschluesselung
	 * @param pk der PrivateKey
	 * @return entschluesselter Text
	 */
	public static String entschluesseln(byte[] chiffrat, PrivateKey pk){
		byte[] dec = null;
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, pk);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			e.printStackTrace();
		}
		
		try {
			dec = cipher.doFinal(chiffrat);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		return new String(dec);
	}
	
	/**
	 * konvertiert den Public-Key als ein String
	 * @param publickey der Public-Key
	 * @return String des Public-Keys
	 */
	public static String schluesselAlsString(PublicKey publickey){
		String sessonkeystring = Base64.getEncoder().encodeToString(publickey.getEncoded());
		return sessonkeystring;
	}
	
	/**
	 * konvertiert den String zu ein Public-Key
	 * @param publickeystring String des Publickeys
	 * @return der Publickey des Strings
	 */
	public static PublicKey stringZuSchluessel(String publickeystring){
		try {
			KeyFactory kf = KeyFactory.getInstance("RSA");
			byte[] chiffre = Base64.getDecoder().decode(publickeystring.getBytes());
			X509EncodedKeySpec ks = new X509EncodedKeySpec(chiffre);
			return kf.generatePublic(ks);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
	}

}

